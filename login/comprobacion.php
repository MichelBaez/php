<?php
    session_start();
    
    if(!isset($_SESSION['Alumno'])){
        header('Location: login.php');
        exit;
    }
    

    $_SESSION['Alumno'] = [
        1 => [
            'num_cta' => '1',
            'nombre' => 'Admin',
            'primer_apellido' => 'General',
            'segundo_apellido' => '',
            'contrasena' => 'adminpass123.',
            'genero' => 'O',
            'fecha_nac' => '25-01-1990'
        ],

        2 => [
            'num_cta' => '2',
            'nombre' => 'Michel',
            'primer_apellido' => 'Báez',
            'segundo_apellido' => 'Cadena',
            'contrasena' => 'contra.',
            'genero' => 'H',
            'fecha_nac' => '12-07-1998'
        ],
    ];

    $cuenta = $_POST['input-cuenta'];
    $password = $_POST['input-password'];

    if (($cuenta === $_SESSION['Alumno'][$cuenta]['num_cta']) && ($_SESSION['Alumno'][$cuenta]['contrasena']==$password)) {
        $_SESSION['id']=$cuenta;
        header('Location: info.php');
        exit;
    }
    else{
        echo "<script>
                alert('USUARIO y/o CONTRASEÑA INCORRECTOS');
                window.location= 'login.php'
            </script>";
        exit;
    }
 
?>
