<?php
session_start();
?>
<html lang="es">
    <head>
        <title>Registrar Alumno</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            body{
                background: #D9DBDF;
                font-family: Arial, Helvetica, sans-serif;
            }
            header{
                background: #007BFF;
                font-family: Arial, Helvetica, sans-serif;
                margin: 15px 30px;
                border-radius: 5px;
            }
            .actual{
                opacity: .6 ;
            }
            header nav{
                width: 100%;
            }
            header nav ul{
                width: 100%;
                overflow: hidden;
                list-style: none;
            }
            header nav ul li{
                float: left;
            }
            header nav ul li a{
                text-decoration: none;
                display: inline-block;
                padding: 15px 20px;
                color: #fff;
            }
            header nav ul li a:hover{
                background: #000;
                opacity: 1;
            }
            .btn{
                background-color: #007BFF;
                border-radius: 5px;
                padding: 7px 15px;
                color: #ffffff;
                font-size: 14px;
                margin-left: 50px;
                border: #393E3E;
                width: 150px;
                height:40px;
            }
            .btn:hover{
                opacity: .8;
                cursor: pointer;
            }
            label{
                margin-left: 25px;
                margin-right: 45px;
                display:inline-block;
                width: 200px;
                font-size: 20px;
                padding: 5px 10px;
            }
            .form-input{
                width: 1200;
                padding: 5px 15px;
                font-size: 20px;
                border-radius: 4px;
                border: #393E3E .5px solid;            
            }
            .form-option{
                margin-left:300px;
            }
        </style>   
    </head>

    <body>
    <?php
        if(!isset($_SESSION['Alumno'])){
            header('Location: login.php');
            exit;
        }
    ?>
        <header>
            <nav>
                <ul>
                    <li><a href="./info.php" class="actual">Home</a></li>
                    <li><a href="./formulario.php">Registrar Alumnos</a></li>
                    <li><a href="./logout.php"  class="actual">Cerrar Sesión</a></li>
                </ul>
            </nav>
        </header>
        <br /><br />
        <form class="formulario" action="registrar.php" method="post">
            <label class="form-label" for="input-cuenta">Número de cuenta</label>
            <input name="input-cuenta" class="form-input" type="text" id="input-cuenta" placeholder="Número de cuenta" required >
            <br /><br />

            <label class="form-label" for="input-nombre">Nombre</label>
            <input name="input-nombre" class="form-input" type="text" id="input-nombre" placeholder="Nombre" required>
            <br /><br />

            <label class="form-label" for="input-primer-apellido">Primer Apellido</label>
            <input name="input-primer-apellido" class="form-input" type="text" id="input-primer-apellido" placeholder="Primer Apellido" required>
            <br /><br />

            <label class="form-label" for="input-segundo-apellido">Segundo Apellido</label>
            <input name="input-segundo-apellido" class="form-input" type="text" id="input-segundo-apellido" placeholder="Segundo Apellido" required>
            <br /><br />

            <label class="form-label" for="input-genero">Género</label>
            <input class="f-option" name="input-genero" type="radio" value="H" id="hombre" required><label  for="hombre">Hombre</label>
            <br />
            <input class="form-option" name="input-genero" type="radio" value="M" id="mujer" required><label for="mujer">Mujer</label>
            <br />
            <input class="form-option" name="input-genero" type="radio" value="O" id="otro" required><label for="otro">Otro</label>
            <br /><br />

            <label class="form-label" for="input-fecha">Fecha de Nacimiento</label>
            <input name="input-fecha" class="form-input" type="date" id="input-fecha" required>
            <br /><br />

            <label class="form-label" for="input-password">Contraseña</label>
            <input name="input-password" class="form-input " type="password" id="input-password" placeholder="Contraseña" required>
            <br /><br /><br />

            <input type='submit' class="btn" value="Registrar"/>

        </form>
    </body>
</html>