<?php
session_start();
?>

<html lang="es">
    <head>
        <title>Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .actual{
                opacity: .6 ;
            }
            body{
                background: #D9DBDF;
                font-family: Arial, Helvetica, sans-serif;                
            }
            header  {
                background: #007BFF;
                font-family: Arial, Helvetica, sans-serif;
                margin: 15px 30px;                
                border-radius: 5px;
            }
            header nav{
                width: 100%;                
            }
            header nav ul{
                width: 100%;
                overflow: hidden;
                list-style: none;
            }
            header nav ul li{
                float: left;
            }
            header nav ul li a{
                text-decoration: none;
                display: inline-block;
                padding: 15px 20px;
                color: #fff;
            }
            header nav ul li a:hover{
                background: #000;
                opacity: 1;
            }
            .titulo{
                margin: 40px 30px;
                padding: 0px 15px;
                font-size: 40px;
                font-weight: bold;
            }
            .usuario, h1 {
                margin: 20px 30px; 
                width: 1400px;   
                            
            }
            .usuario td{
                border-radius: 10px;
                padding: 15px 15px 0px 15px;
                border: 2px solid #6F6F70;
            }
            .listado td {
                padding: 8px 8px 8px 8px;
                border-top: 1px solid #6F6F70;
                border-bottom: 1px solid #6F6F70;
            }
            .listado{
                margin: 20px 30px;
                width: 1400px;
                border-collapse: collapse;
            }
            .listado tr:hover{
                background: #A8A8A9;
            }
            .listado td strong{
                font-size: 18px;
            }
        </style>
    </head>

    <body>
    <?php
        if(!isset($_SESSION['Alumno'])){
            header('Location: login.php');
            exit;
        } 
    ?>   
        <header>
            <nav>
                <ul>
                    <li><a href="./info.php">Home</a></li>
                    <li><a href="./formulario.php" class="actual">Registrar Alumnos</a></li>
                    <li><a href="./logout.php" class="actual">Cerrar Sesión</a></li>
                </ul>
            </nav>
        </header>
        <br />
        <div class="titulo">Usuario Autenticado</div>

        <div class="info">
            <table class="usuario"> <?php 
                $id = $_SESSION['id'];
                echo 
                    "<tr><td style='background-color:#A8A8A9'><h2>" .
                        $_SESSION['Alumno'][$id]['nombre'] . " " . $_SESSION['Alumno'][$id]['primer_apellido'] . " ". $_SESSION['Alumno'][$id]['segundo_apellido'] .
                    "</h2></td></tr>" .
                    "<tr><td><h3>
                        Información </h3>
                        Número de cuenta: " . $_SESSION['Alumno'][$id]['num_cta'] . "<br /><br />
                        Fecha de nacimiento: " .  $_SESSION['Alumno'][$id]['fecha_nac'] . "<br /><br />
                    </td></tr>"
            ?> </table>    

            <br /><br /><br />
            <h1>Datos Guardados:</h1>
            <nav>
                <table class="listado">
                    <tr>
                        <td><strong>#</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Fecha de Nacimiento</strong></td>
                    </tr>
                    <?php
                        for($i=1; $i<=sizeof($_SESSION['Alumno']); $i++){
                            echo 
                            "<tr>
                                <td style='font-weight: bold'>" . $_SESSION['Alumno'][$i]['num_cta'] . "</td>
                                <td>".  $_SESSION['Alumno'][$i]['nombre'] . " " .
                                        $_SESSION['Alumno'][$i]['primer_apellido'] . " " .
                                        $_SESSION['Alumno'][$i]['segundo_apellido'] . "</td>
                                <td>" . $_SESSION['Alumno'][$i]['fecha_nac'] . "</td>
                            </tr>";
                        }
                    ?>
                </table>
            </nav>


        </div>
    </body>
</html>