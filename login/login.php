<?php
session_start();
?>
<html lang="es">
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body{
                background: #007BFF;
            }
            .container{
                font-family:Arial, Helvetica, sans-serif ;
                background: #D9DBDF;
                border: #393E3E 3px solid;
                border-radius: 7px;
                width: 500px;
                height: 275px;
                margin: 200px auto ;
            }
            .form-label, .form-input{
                width: 400;
                margin-left: 45px;
                padding: 5px 10px;
                border-radius: 7px;
                font-size: 20px;
            }
            center{
                font-size: 28px;
                margin: 15px;
            }
            .btn{
                background-color: #007BFF;
                border-radius: 10px;
                padding: 7px 15px;
                margin-left: 50px;
                color: #ffffff;
                font-size: 14px;
            }
            .btn:hover{
                opacity: .8;
            }
        </style>
    </head>

    <body>
        <div class="container">
                <form action="comprobacion.php" action="info.php" method="POST" class="formu">
                    
                    <center>Login</center>
                    <label class="form-label" for="input-cuenta">Número de cuenta</label>
                    <br />
                    <input name="input-cuenta" class="form-input " type="text" id="input-cuenta" required>
                    <br /><br />
                    <label class="form-label" for="input-password">Contraseña</label>
                    <br />
                    <input name="input-password" class="form-input " type="password" id="input-password">
                    <br /><br />
                    <input type='submit' class="btn" value="Entrar"/>
                </form>
        </div>
    </body>
</html>