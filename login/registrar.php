<?php
    session_start();
    if(!isset($_SESSION['Alumno'])){
        header('Location: login.php');
        exit;
    }
    $cuenta = $_POST['input-cuenta'];

    if ($cuenta == $_SESSION['Alumno'][$cuenta]['num_cta']){
        echo "<script>
                alert('NÚMERO DE CUENTA YA REGISTRADA');
                window.location= 'formulario.php'
            </script>";
            exit;
    }else{
        $_SESSION['Alumno'][$cuenta]['num_cta'] = $cuenta;
        $_SESSION['Alumno'][$cuenta]['nombre'] = $_POST['input-nombre']; 
        $_SESSION['Alumno'][$cuenta]['primer_apellido'] = $_POST['input-primer-apellido'];
        $_SESSION['Alumno'][$cuenta]['segundo_apellido'] = $_POST['input-segundo-apellido'];
        $_SESSION['Alumno'][$cuenta]['contrasena'] = $_POST['input-password'];
        $_SESSION['Alumno'][$cuenta]['genero'] = $_POST['input-genero'];
        $_SESSION['Alumno'][$cuenta]['fecha_nac'] = $_POST['input-fecha'];
        echo "<script>
                alert('EL ALUMNO HA SIDO REGISTRADO');
                window.location= 'info.php'
            </script>";
    }
    
?>



