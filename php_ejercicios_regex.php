<html>
    <head>
        <title>Expresiones Regulares</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            body{
                text-align: center;
            }
            div{
                margin-left: 550px;
                text-align: left;
            }
        </style>
    </head>

    <body>
    <h2>Realizar una expresión regular que detecte emails correctos.</h2>
    <h3>'/[A-Za-z]+@[a-z]+\.[a-z]+/'</h3>
    <?php
        $listaTextos = array(
            '¡Hola Mundo!',
            'miCorreo@gmail.com',
            'La teoría del Big Bang',
            'correoFalso@yahoo.es',
            '+34 91 123 456 789',
            'estoNOesUnCorreoNoTieneArroba.com ',
            'RaMoN@jarroba.com'
        );
        $patron_correo = '/[A-Za-z]+@[a-z]+\.[a-z]+/';
        echo '<div>';
        foreach ($listaTextos as $correo) {
            $esCoincidente = preg_match($patron_correo, $correo);
            if ($esCoincidente) {
                echo  '<strong> Correo Válido </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $correo . '<br />' ;
            }else{ 
                echo '<strong> Correo Inválido </strong>&nbsp;&nbsp;&nbsp;&nbsp;'. $correo . '<br />' ;
            }
        }
        echo '</div>';
    ?>

    <br>
    <h2>Realizar una expresion regular que detecte Curps Correctos<br />
    'ABCD123456EFGHIJ78'.</h2>
    <h3>'/[A-Z]{4}[0-9]{6}[HM]{1}[A-Z]{5}[0-9]{2}/'</h3>
    <?php
        $listaCurp = array(
            'EstoNoEsUnCURP628',
            'MJED250148MIEJSM12',
            '100 años de soledad',
            'BACD980712HDFZDS04',
            '+34 91 123 456 789',
            'DJEM010205HKIEJD36',
            'HolaComoEstan'
        );
        $patron_curp = '/[A-Z]{4}[0-9]{6}[HM]{1}[A-Z]{5}[0-9]{2}/';
        echo '<div>';
        foreach ($listaCurp as $curp) {
            $siEsCurp = preg_match($patron_curp, $curp);
            if($siEsCurp){
                echo '<strong>SI es CURP: </strong>&nbsp;&nbsp;&nbsp;&nbsp;' . $curp . '<br/>';
            }else{
                echo '<strong>NO es CURP: </strong>&nbsp;&nbsp;' . $curp . '<br/>'  ;
            }
        }
        echo '</div>';
    ?>
    <br>
    <h2>Realizar una expresion regular que detecte palabras de longitud mayor a 50<br/>
    formadas solo por letras.</h2>
    <h3>'/[A-Za-z]{49,}/'</h3>
    <?php
        $listaPalabra = array(
            'qwertyuioplkjhgfdsaZXCVBNMqwertyuioplkjhgfkhkDSAZXCVBN',
            'MfkqehoifeddfweJKGKGKGKGHFFHGFHCGFJGCGFJFKTGK',
            'efbWJVBEFKJDHMCHAKLSDJKjdlhaljfjfheuofhowe9fefhpef68w4e+6f+ew',
            'WFCWJEGFCIUGJFBWJHlkhelahflahoHLJHhJhLlHLHohLJHohJKHkjgKBkgKBKgkvYFyfxt',
            '6568478465469846789746879849874964456789',
            'DJEMefwfefwekfhoeahfPPIJEAFQEegfjgugJLHLhlkhKBLgfutxryS vpvOIUCTXTYEÑ',
            'HolaComoEstan'
        );
        $patron_palabra = '/[A-Za-z]{49,}/';
        echo '<div>';
        foreach ($listaPalabra as $palabra) {
            $siEsPalabra = preg_match($patron_palabra, $palabra);
            if($siEsPalabra){
                echo '<strong>SI tiene más de 50 letras&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>' . $palabra . '<br/>';
            }else{
                echo '<strong>NO tiene más de 50 letras&nbsp;&nbsp;&nbsp;&nbsp;</strong>' . $palabra . '<br/>';
            }
        }
        echo '</div>';
    ?>

    <br>
    <h2>Crea una funcion para escapar los simbolos especiales.</h2>
    <?php
        $especial = '-Hola! Como estan;??';
        $nuevo_texto = preg_quote($especial, '/');
        echo $especial . "<br/>";
        echo $nuevo_texto . "<br/>";
    ?>
    <br>
    <h2>Crear una expresion regular para detectar números decimales.</h2>
    <h3>'/[0-9]+\.[0-9]+/'</h3>
    <?php
        $listaNumero = array(
            '10.25',
            '125046',
            '36521.25125',
            '5566332148',
            '1021453.3251',
            '5215666.325218856'
        );
        $patron_decimal = '/[0-9]+\.[0-9]+/';
        echo '<div>';
        foreach ($listaNumero as $decimal) {
            $siEsPalabra = preg_match($patron_decimal, $decimal);
            if($siEsPalabra){
                echo '<strong>SI es número decimal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>' . $decimal . '<br/>';
            }else{
                echo '<strong>NO es número decimal&nbsp;&nbsp;&nbsp;&nbsp;</strong>' . $decimal . '<br/>';
            }
        }
        echo '</div>';
    ?>
    </body>
</html>



